# Github Looker

Github Looker is a project which help getting basic information for given user's Github repository.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. Java 8
2. Installed `docker`

### Building

Run following command to build docker images for this project:

***Windows***
```bash
mvnw.cmd clean install
```

***Unix based system***
```bash
./mvnw clean install
```

### Running

Run command to start application (within project directory):

```bash
docker run -p"8080:8080" kamil_czyznielewski/github-looker:latest
```

Application should start and be exposed on port 8080. Check `/health` endpoint: `http://localhost:8080/health`
You should see following response:
```
200 OK
{
    status: "UP"
}
```
